#!/bin/bash
# Import library
source ConstValues.sh

# User config
UNITY_VERSION="2019.3.12f1"
PROJECT_PATH="/Users/ittim/Projects/empty"
LOG_FILE_PATH=$PROJECT_PATH/build.log
OUTPUT_PATH="/Users/ittim/Desktop/"

# Program parameter
UNITY_PATH=$UNITY_PREFIX$UNITY_VERSION$UNITY_SURFFIX
BUILD_METHOD="BuildScript.Build 123456"

InvokeUnityBuild()
{
    
    echo "InvokeUnityBuild string version"
    $UNITY_PATH -quit -batchmode -projectPath $PROJECT_PATH -executeMethod $BUILD_METHOD -logFile $LOG_FILE_PATH -destinationPath $OUTPUT_PATH
    less $LOG_FILE_PATH
    echo "InvokeUnityBuild end"
}

InvokeUnityBuild

